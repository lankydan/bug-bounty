<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Welcome to the Big Blockchain Bug Bounty Bake Off 

## OPPORTUNITY
   * Hands on [Corda](https://corda.net) training
   * Learn how to use [Cordite](https://gitlab.com/cordite/cordite) to issue your own Tokens and form your own DAOs
   * Learn how to create and host a Cordite node
   * Develop a new exciting open source CordApp - [Cordite Bounty Scheme](docs/CorditeBountyScheme.md)
   * Join a fast growing open source [community](https://cordaledger.slack.com/messages/cordite/)

## PURPOSE
   * Develop a sustainable bounty scheme for contributions to the Corda community
   * Test different schemes and economic models
   * [Storm and Norm](https://en.wikipedia.org/wiki/Tuckman%27s_stages_of_group_development) to a single team working on a single sustainable open source app

## TO GET STARTED
   * Join [#cordite](https://cordaledger.slack.com/messages/cordite/) on the Corda slack to communicate with others and get help
   * Ensure you have done the [pre-reqs](docs/prework.md)
   * We will do the [quick start exercise](docs/exercise.md) as part of the week one meetup
   * Read and understood the [Cordite Bounty Scheme aims and rules](docs/CorditeBountyScheme.md) which is what we are using to judge
   * Work through the [Bug Bounty cordApp](docs/bb-cordapp.md) source code
   * and the [GUI](docs/gui.md) source code
   * Enjoy the collection of [resources](docs/resources.md) we have pulled together for you. This is the good stuff.
   * Please [raise any issues](https://gitlab.com/cordite/bug-bounty/issues/new) you find

## WHAT IF SOMETHING DOES NOT WORK?
We encourage you to raise any issues/bugs you find with this bug-bounty. Please follow the below steps before raising issues:
   1. Check on the [Issues backlog](https://gitlab.com/cordite/bug-bounty/issues) to make sure an issue on the topic has not already been raised
   2. Post your question on the #cordite channel on [Corda slack](https://cordaledger.slack.com/messages/cordite/)
   3. If none of the above help solve the issue, [raise an issue](https://gitlab.com/cordite/bug-bounty/issues/new?issue) following the contributions guide

## WHAT IF EVERYTHING WORKS LIKE A DREAM?
We encourage you to join the #cordite channel on [Corda slack](https://cordaledger.slack.com/messages/cordite/) and help others who might be having a nightmare!  

# Bug Bounty Repo Structure
This set up is basically a fork of the [corda kotlin template](https://github.com/corda/cordapp-template-kotlin).  
It contains:

```bash
├── bb-cordapp # Bug Bounty cordApp
├── bb-cordapp-contracts-states # This is empty at the mo - contracts and states can go here later
├── config # Config files for dev and test
├── docs # Collection of docs to get you started
├── etc # Collection of useful scripts for building gitlab CI docker image, interacting with Bug Bounty NMS
├── gradle*  # Everything gradle
├── gui # Bug Bounty GUI
├── lib # The Quasar.jar in this directory is for runtime instrumentation of classes by Quasar
├── LICENSE # All contributions to this repo are covered by this LICENSE
├── README.md # This document
├── Dockerfile # Dockerfile to assemble bb-cordapp into a deployable node
└── docker-compose.yml # Use to start a node and public p2p address
```

# Quick Start
For those who have ignored everything above  
```bash
export CORDITE_LEGAL_NAME="O=$(git config user.name) , OU=Corda Code Club, L=London, C=GB"
export CORDITE_P2P_PORT=$(($RANDOM % 10000 + 10000)) # A random port between 10000-19999 
export CORDITE_TAG=local
./gradlew clean test
docker-compose build
docker-compose up -d
./gradlew gui:run
```
At the end of this, you should have:

 * a docker node running with corda, cordite and bug bounty running in it
 * a serveo container that is mapping outside traffic to your node with a well known address
 * a new directory ```/node``` which contains your certs, db and node-info. Do not delete this. docker-compose will re-use these each time the node starts.
 * A gui to interact with node

## How do I contribute?
We welcome contributions both technical and non-technical with open arms! There's a lot of work to do here. The [Contributing Guide](https://gitlab.com/cordite/bug-bounty/blob/master/contributing.md) provides more information on how to contribute.

## Who is behind Cordite?
Cordite is being developed by a group of financial services companies, software vendors and open source contributors. The project is hosted on GitLab. 

## What open source license has this been released under?
All software in this repository is licensed under the Apache License, Version 2.0 (the "License"); you may not use this software except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

