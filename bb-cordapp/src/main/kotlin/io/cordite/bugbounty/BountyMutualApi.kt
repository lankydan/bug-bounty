/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.bugbounty

import io.cordite.commons.utils.transaction
import io.cordite.dao.DaoApi
import io.cordite.dao.DaoApiImpl
import io.cordite.dao.core.DaoKey
import io.cordite.dao.data.DataHelper
import io.cordite.dao.proposal.ProposalState
import io.cordite.mutual.teapot.TeapotProposal
import io.vertx.core.Future
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub

interface BountyApi: DaoApi {
  fun createTeapotProposal(teapot: CordaX500Name, daoKey: DaoKey): Future<ProposalState<TeapotProposal>>
  fun teapotProposalsFor(daoKey: DaoKey): List<ProposalState<TeapotProposal>>
}

class BountyApiImpl(val serviceHub: AppServiceHub): BountyApi, DaoApi by DaoApiImpl(serviceHub) {

  override fun createTeapotProposal(teapot: CordaX500Name, daoKey: DaoKey): Future<ProposalState<TeapotProposal>> {
    val teapotParty = findNode(teapot)
    return createProposal(TeapotProposal(teapotParty), daoKey)
  }

  override fun teapotProposalsFor(daoKey: DaoKey): List<ProposalState<TeapotProposal>> {
    return serviceHub.transaction {
      DataHelper.proposalStatesFor(daoKey, serviceHub)
    }
  }

  private fun findNode(node: CordaX500Name): Party {
    return serviceHub.networkMapCache.getPeerByLegalName(node) ?: throw RuntimeException("could not find node $node")
  }


}