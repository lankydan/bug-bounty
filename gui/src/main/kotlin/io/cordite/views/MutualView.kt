/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.MutualViewModel
import io.cordite.viewmodel.MutualModel
import javafx.geometry.Pos
import tornadofx.*

class MutualView : View("Mutuals") {
  val mutualViewModel: MutualViewModel by inject()

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }
    center {
      stackpane {
        tableview(mutualViewModel.mutualModels) {
          readonlyColumn("Name", MutualModel::name)
          readonlyColumn("#Members", MutualModel::memberCount)
          readonlyColumn("Min #Members", MutualModel::minMembers)
          readonlyColumn("Key",MutualModel::key)

          onSelectionChange {
            println(it)
          }

          smartResize()
        }
      }
    }
    bottom {
      hbox(10, Pos.CENTER) {
        addClass(Styles.content)
        button("create dm") {
          action {
            find<CreateMutualView>().openModal()
          }
        }

        button("join dm") {
          action {
            find<JoinMutualView>().openModal()
          }
        }
      }
    }
  }
}