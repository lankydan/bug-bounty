/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.AccountModel
import io.cordite.viewmodel.LedgerViewModel
import io.cordite.viewmodel.TokenAmountModel
import io.cordite.viewmodel.TokenModel
import javafx.geometry.Pos
import tornadofx.*

class LedgerView : View("Ledger") {
  val ledgerViewModel: LedgerViewModel by inject()

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }
    center {
      vbox {
        tableview(ledgerViewModel.accounts) {
          readonlyColumn("Account", AccountModel::name)

          onSelectionChange {
            if (it != null) {
              ledgerViewModel.accountSelected(it)
            }
          }

          ledgerViewModel.accounts.onChange {
            if (ledgerViewModel.selectedAccount == null) {
              selectFirst()
            }
          }

          contextmenu {
            item("Copy Address").action {
              selectedItem?.apply { clipboard.putString("$address") }
            }
          }

          smartResize()
        }
      }.addClass(Styles.content)
    }
    right {
      vbox {
        tableview(ledgerViewModel.amounts) {
          readonlyColumn("Token", TokenAmountModel::name)
          readonlyColumn("Balance", TokenAmountModel::balance)
          smartResize()
        }
      }.addClass(Styles.content)
    }
    left {
      vbox{
        tableview(ledgerViewModel.tokens) {
          readonlyColumn("Symbol", TokenModel::symbol)
          readonlyColumn("Exp", TokenModel::exponent)
          smartResize()
        }
      }.addClass(Styles.content)
    }
    bottom {
      hbox(10, Pos.CENTER) {
        addClass(Styles.content)
        button("refresh") {
          action {
            runAsync {
              ledgerViewModel.refreshDataUntilRxAvailableForLedger()
            }
          }
        }
        button("create token") {
          action {
            find<CreateTokenView>().openModal()
          }
        }
        button("issue tokens") {
          action {
            find<IssueTokensView>().openModal()
          }
        }
        button("transfer tokens") {
          action {
            find<TransferTokensView>().openModal()
          }
        }
        button("create account") {
          action {
            find<CreateAccountView>().openModal()
          }
        }
      }
    }
  }
}