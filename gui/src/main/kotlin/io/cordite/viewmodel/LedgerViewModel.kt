/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.viewmodel

import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.token.TokenType
import io.cordite.model.LedgerModel
import io.cordite.model.NetworkModel
import io.cordite.util.then
import io.vertx.core.AsyncResult
import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import net.corda.core.contracts.Amount
import tornadofx.*

class LedgerViewModel: Controller() {

  private val ledgerApi = LedgerModel.ledgerApi

  val accounts = mutableListOf<AccountModel>().observable()
  val amounts = mutableListOf<TokenAmountModel>().observable()
  val tokens = mutableListOf<TokenModel>().observable()

  var selectedAccount: AccountModel? = null

  init {
    refreshDataUntilRxAvailableForLedger()
//    ledgerApi.listenForTransactions(emptyList()).subscribe {
//      // hacky but ok for the mo - realise this is a bit ironic given the name...
//      println("tx received - refreshing data")
//      refreshDataUntilRxAvailableForLedger()
//    } // comment back in when everything is upgraded to 3.2.6 braid
  }

  private fun updateTokensList() {
    ledgerApi.listTokenTypes().then {
      tokens.clear()
      it.forEach { that ->
        tokens.add(TokenModel(that.descriptor))
      }
    }
  }

  fun refreshDataUntilRxAvailableForLedger() {
    updateAccountList()
    updateTokensList()
    updateTokensForAccount()
  }

  private fun updateAccountList() {
    ledgerApi.listAccounts().then {
      accounts.clear()
      it.forEach { that ->
        accounts.add(AccountModel(that))
      }
    }
  }

  private fun updateTokensForAccount() {
    val accountModel = selectedAccount ?: return
    ledgerApi.balanceForAccount(accountModel.name).then {
      amounts.clear()
      it.forEach { that ->
        amounts.add(TokenAmountModel(that))
      }
    }
  }

  fun accountSelected(accountModel: AccountModel) {
    this.selectedAccount = accountModel
    updateTokensForAccount()
  }

  fun createToken(newToken: CreateToken) {
    println("creating token ${newToken.symbol}")
    ledgerApi.createTokenType(newToken.symbol, newToken.exponent, NetworkModel.notary.name).setHandler(this::handler)
  }

  fun issueTokens(item: NewIssuance) {
    println("issuing ${item.amount} ${item.descriptor.symbol} tokens to ${item.account.name}")
    ledgerApi.issueToken(item.account.name, item.amount, item.descriptor.symbol, item.description, NetworkModel.notary.name).setHandler(this::handler)
  }

  private fun <T> handler(result: AsyncResult<T>) {
    if (result.succeeded()) {
      refreshDataUntilRxAvailableForLedger()
    } else {
      result.cause().printStackTrace()
    }
  }

  fun transferTokens(item: Transfer) {
    ledgerApi.transferToken(item.amount, item.descriptor.uri, item.fromAccount.name, item.toAccountUri, item.description, NetworkModel.notary.name).setHandler(this::handler)
  }

  fun createAccount(item: CreateAccount) {
    ledgerApi.createAccount(item.name, NetworkModel.notary.name).setHandler(this::handler)
  }
}

class AccountModel(private val account: Account.State) {
  val name: String get() = account.address.accountId
  val address: String get() = account.address.uri
}

class TokenAmountModel(private val amount: Amount<TokenType.Descriptor>) {
  val name: String get() = amount.token.symbol
  val balance: String get() = amount.toDecimal().toString()
}

// TODO surely these three are the same thing - at the very least they could be two?
class TokenModel(private val token: TokenType.Descriptor) {
  val symbol: String get() = token.symbol
  val exponent: Int get() = token.exponent
  val uri: String get() = token.uri
}

class CreateToken {
  var symbol: String by property<String>()
  fun symbolProperty() = getProperty(CreateToken::symbol)

  var exponent: Int by property<Int>()
  fun exponentProperty() = getProperty(CreateToken::exponent)
}

class CreateTokenViewModel: ItemViewModel<CreateToken>(CreateToken()) {
  val symbol: StringProperty = bind{ item?.symbolProperty() }
  val exponent: Property<Int> = bind{ item?.exponentProperty() }
}

class NewIssuance {
  var account: AccountModel by property<AccountModel>()
  fun accountProperty() = getProperty(NewIssuance::account)

  var descriptor: TokenModel by property<TokenModel>()
  fun descriptorProperty() = getProperty(NewIssuance::descriptor)

  var amount: String by property<String>()
  fun amountProperty() = getProperty(NewIssuance::amount)

  var description: String by property<String>()
  fun descriptionProperty() = getProperty(NewIssuance::description)
}

class NewIssuanceViewModel: ItemViewModel<NewIssuance>(NewIssuance()) {
  val account: Property<AccountModel> = bind{ item?.accountProperty() }
  val descriptor: Property<TokenModel> = bind{ item?.descriptorProperty() }
  val amount: StringProperty = bind{ item?.amountProperty() }
  val description: StringProperty = bind { item?.descriptionProperty() }
}

class Transfer {
  var fromAccount: AccountModel by property<AccountModel>()
  fun fromAccountProperty() = getProperty(Transfer::fromAccount)

  var toAccountUri: String by property<String>()
  fun toAccountUriProperty() = getProperty(Transfer::toAccountUri)

  var descriptor: TokenModel by property<TokenModel>()
  fun descriptorProperty() = getProperty(Transfer::descriptor)

  var amount: String by property<String>()
  fun amountProperty() = getProperty(Transfer::amount)

  var description: String by property<String>()
  fun descriptionProperty() = getProperty(Transfer::description)
}

class TransferViewModel: ItemViewModel<Transfer>(Transfer()) {
  val fromAccount: Property<AccountModel> = bind{ item?.fromAccountProperty() }
  val toAccountUri: Property<String> = bind{ item?.toAccountUriProperty() }
  val descriptor: Property<TokenModel> = bind{ item?.descriptorProperty() }
  val amount: StringProperty = bind{ item?.amountProperty() }
  val description: StringProperty = bind { item?.descriptionProperty() }
}

class CreateAccount {
  var name: String by property<String>()
  fun nameProperty() = getProperty(CreateAccount::name)
}

class CreateAccountViewModel: ItemViewModel<CreateAccount>(CreateAccount()) {
  val name: Property<String> = bind{ item?.nameProperty() }
}

