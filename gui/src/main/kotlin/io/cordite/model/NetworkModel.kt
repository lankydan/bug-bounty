/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.model

import io.bluebank.braid.client.BraidClientConfig
import io.bluebank.braid.client.BraidCordaClient
import io.bluebank.braid.corda.services.SimpleNetworkMapService
import net.corda.core.identity.Party
import java.net.URI

// convert to object and deal with lifecycle later
object NetworkModel {

  val networkServiceURI = URI("${UtterHackery.corditeProtocol}://${UtterHackery.corditeHost}:${UtterHackery.corditePort}/api/network/braid")
  val networkClient = BraidCordaClient(BraidClientConfig(serviceURI = networkServiceURI, trustAll = true, verifyHost = false), VertxHelper.vertx)
  val networkApi = networkClient.bind(SimpleNetworkMapService::class.java)
  val notary = networkApi.notaryIdentities().first()
  val me = networkApi.myNodeInfo().legalIdentities.first()

  fun listParties(): List<Party>  {
    return networkApi.allNodes().map{ it.legalIdentities.first() }
  }

}