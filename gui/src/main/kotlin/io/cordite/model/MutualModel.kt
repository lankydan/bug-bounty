/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.model

import io.bluebank.braid.client.BraidClientConfig
import io.bluebank.braid.client.BraidCordaClient
import io.cordite.dao.DaoApi
import java.net.URI

// convert to object and deal with lifecycle later
object MutualModel {

  val mutualServiceURI = URI("${UtterHackery.corditeProtocol}://${UtterHackery.corditeHost}:${UtterHackery.corditePort}/api/bounty/braid")
  val mutualClient = BraidCordaClient(BraidClientConfig(serviceURI = mutualServiceURI, trustAll = true, verifyHost = false), VertxHelper.vertx)
  val mutualApi = mutualClient.bind(DaoApi::class.java)

}