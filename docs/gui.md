<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# GUI

This uses [tornadofx](https://tornadofx.io) to create a relatively simple, frankly hacked together, gui that can be used
to drive your nodes.

```bash
├── gui
│   ├── build.gradle
│   └── src
│       └── main
│           ├── kotlin
│           │   └── io
│           │       └── cordite
│           │           ├── CorditeApp.kt
│           │           ├── Styles.kt
│           │           ├── model
│           │           │   ├── LedgerModel.kt
│           │           │   ├── MutualModel.kt
│           │           │   ├── NetworkModel.kt
│           │           │   ├── UtterHackery.kt
│           │           │   └── VertxHelper.kt
│           │           ├── util
│           │           │   └── FutureHelpers.kt
│           │           ├── viewmodel
│           │           │   ├── LedgerViewModel.kt
│           │           │   ├── MutualViewModel.kt
│           │           │   └── ProposalViewModel.kt
│           │           └── views
│           │               ├── CreateAccountView.kt
│           │               ├── CreateEconomicsModelDataProposalView.kt
│           │               ├── CreateIssuanceProposalView.kt
│           │               ├── CreateMutualView.kt
│           │               ├── CreateNormalProposalView.kt
│           │               ├── CreateTokenView.kt
│           │               ├── IssueTokensView.kt
│           │               ├── JoinMutualView.kt
│           │               ├── LedgerView.kt
│           │               ├── MainView.kt
│           │               ├── MutualView.kt
│           │               ├── ProposalView.kt
│           │               └── TransferTokensView.kt
│           └── resources
│               └── logo-watermark-200.png
```

So the next step is to get your gui running.  You can choose whether to do this using gradle or intellij.  The 
obvious advantage of the second approach is the ability to debug.

For the moment, let's just use gradle:

```bash
./gradlew gui:run
```

## Create a run config for the gui in intellij
 * run -> edit configurations
 * add a TornadoFX configuration
   * type: application
   * app class: io.cordite.CorditeApp
   * working dir: $MODULE_WORKING_DIR$
   * use classpath of module: gui_main