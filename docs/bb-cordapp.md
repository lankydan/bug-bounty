<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Bug Bounty Cordapp
This is a tiny cordapp that extends the cordite digital mutual code with a simple Teapot plugin.  The idea is that you
can propose that someone is a teapot.  Very important stuff, in honour of the [418](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/418)
I'm a teapot http error code!

It uses:

 * the [cordite](https://gitlab.com/cordite/cordite) cordapps
 * [braid](https://gitlab.com/bluebank/braid)
 * [corda](https://docs.corda.net) version 3.3

It is based on the [corda kotlin template](https://github.com/corda/cordapp-template-kotlin).

```bash
├── bb-cordapp
│   ├── build.gradle
│   └── src
│       ├── main
│       │   └── kotlin
│       │       └── io
│       │           └── cordite
│       │               ├── bugbounty
│       │               │   ├── BountyMutualApi.kt
│       │               │   └── BountyServer.kt
│       │               └── mutual
│       │                   └── teapot
│       │                       └── TeapotProposal.kt
│       └── test
│           └── kotlin
│               ├── com
│               │   └── template
│               │       └── NodeDriver.kt
│               └── io
│                   └── cordite
│                       ├── mutual
│                       │   └── teapot
│                       │       └── TeapotIntegrationTest.kt
│                       └── test
│                           └── utils
│                               ├── BraidClientHelper.kt
│                               ├── BraidPortHelper.kt
│                               ├── DaoTestHelper.kt
│                               ├── GenericTestUtils.kt
│                               └── TestConstants.kt
├── bb-cordapp-contracts-states
│   ├── build.gradle
│   └── src
│       └── main
│           └── kotlin
│               └── com
│                   └── template
│                       └── StatesAndContracts.kt
```


```bash
export CORDITE_LEGAL_NAME="O=<YOUR GITLAB USERNAME>, OU=Corda Code Club, L=London, C=GB"
export CORDITE_P2P_PORT=<YOUR RESERVED PORT>
export CORDITE_TAG=local
./gradlew clean test
docker-compose build
docker-compose up
```

You can find some instructions re ports [here](docs/publicP2Paddress.md)

At the end of this, you should have:

 * a docker node running with corda, cordite and bug bounty running in it
 * a serveo container that is mapping outside traffic to your node with a well known address

Note that the corda node is listening to p2p traffic from the outside world, and it's running braid on port 8080.
The latter is so that we can talk to our node form our gui.  Speaking of which...

# Recovering from stuff ups

It's actually quite fiddly to get everything right and we regularly get ourselves into a complete mess!

Current steps to reset your env (noting that this may well roger other people's envs - they may be mid flow with you e.g.):

 * stop everything
 * make sure everything dockery has gone ```etc/docker/dockerStopAll.sh && etc/docker/dockerRmAll.sh```
 * clean out all the stuff in the node dir ```rm -rf node/*```
 * clean up your entry in the nms
 
```bash
etc/nms/get.sh
# copy your node id
etc/nms/deleteNode.sh <node id>
```

After having done this you are hopefully ready to restart everything.  We seem to get into all manner of issues at this
stage with other nodes having old keys on and things.  It would be good to capture these issues together so that we can 
begin to understand how to make this upgrade/general usage process a bit more robust.